#!/bin/sh
#################################################################
# Copyright (c) 2019 YottaDB LLC. and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################
cat >$1_cgo.go <<EOF
// package main
/*
#include <libyottadb.h>
#include <inttypes.h>
int $1(uint64_t tptoken, ydb_buffer_t *errstr, void *tpfnparm)
int $1_cgo(uint_t tptoken, ydb_buffer_t *errstr, void *tpfnparm)
{
	return $1(tptoken, errstr, tpfnparm);
}
*/
import "C"

func GetRtn$1() {
     return C.$1_cgo()
}
EOF
