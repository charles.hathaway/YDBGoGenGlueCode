//////////////////////////////////////////////////////////////////
//								//
// Copyright (c) 2019 YottaDB LLC. and/or its subsidiaries.	//
// All rights reserved.						//
//								//
//	This source code contains the intellectual property	//
//	of its copyright holder(s), and is made available	//
//	under a license.  If you do not know the terms of	//
//	the license, please stop and do not read further.	//
//								//
//////////////////////////////////////////////////////////////////

package main

import (
	"flag"
	"fmt"
	"os"
)

func main() {
	routineNamePtr := flag.String("func", "", "the function name we are generating a template for")
	packageNamePtr := flag.String("pkg", "", "the package name we are generating a template for")
	flag.Parse()
	if *routineNamePtr == "" || *packageNamePtr == "" {
		fmt.Printf("Missing argument; please give -func and -pkg\n")
		os.Exit(2)
	}
	pkg := *packageNamePtr
	rtn := *routineNamePtr
	var tmpl = `
package %s

import "unsafe"

/*
#include <libyottadb.h>
#include <inttypes.h>
int %s(uint64_t tptoken, ydb_buffer_t *errstr, void *tpfnparm);
int %s_cgo(uint64_t tptoken, ydb_buffer_t *errstr, void *tpfnparm) {
    return %s(tptoken, errstr, tpfnparm);
}
*/
import "C"

func Get%sCgo() unsafe.Pointer {
    return C.%s_cgo
}
`
	f, err := os.Create(rtn + "_cgo.go")
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(f, tmpl, pkg, rtn, rtn, rtn, rtn, rtn)
}
